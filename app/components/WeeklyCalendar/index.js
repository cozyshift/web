/**
*
* WeeklyCalendar
*
*/

import React from 'react'

import moment from 'moment'
import 'moment-range'
import _ from 'lodash'

import styles from './styles.css'

class WeeklyCalendar extends React.Component {

  constructor(props) {
    super(props)
    this.handleSelection = this.handleSelection.bind(this)
    this.startSelection = this.startSelection.bind(this)
    this.endSelection = this.endSelection.bind(this)
    this.extendSelection = this.extendSelection.bind(this)
  }

  handleSelection(event) {
    if (this.props.selecting) {
      return this.endSelection(event)
    }

    return this.startSelection(event)
  }

  startSelection({ currentTarget }) {
    const start = currentTarget.dataset.start
    const end = currentTarget.dataset.end

    if (this.props.startSelection) {
      this.props.startSelection({
        start: moment(start),
        end: moment(end),
      })
    }
  }

  endSelection({ currentTarget }) {
    const end = moment(currentTarget.dataset.end)

    if (
      this.props.endSelection &&
      end.isAfter(this.props.selectionStart)
    ) {
      this.props.endSelection({
        end,
      })
    }
  }

  extendSelection({ currentTarget }) {
    const end = moment(currentTarget.dataset.end)

    if (!this.props.selecting) return

    if (
      this.props.extendSelection &&
      end.isAfter(this.props.selectionStart)
    ) {
      this.props.extendSelection({
        end,
      })
    }
  }

  isDayOpen(day) {
    if (this.props.openingTimes[day.isoWeekday() - 1].length === 0) {
      return false
    }

    if (
      this.props.closedDays.length !== 0
      && _.findIndex(this.props.closedDays, closeDay => moment(closeDay).isSame(day)) !== -1
    ) {
      return false
    }

    return true
  }

  render() {
    const {
      week,
      openingTimes,
      selectionStart,
      selectionEnd,
      selecting,
    } = this.props

    const latestOpenTime = _.maxBy(_.flattenDeep(openingTimes), x => parseInt(_.split(x, ':')[0], 10))
    const firstOpenTime = _.minBy(_.flattenDeep(openingTimes), x => parseInt(_.split(x, ':')[0], 10))

    const getHourClass = hour => {
      if (
        selecting &&
        hour.isSameOrAfter(selectionStart) &&
        hour.isBefore(selectionEnd)
      ) {
        return styles.selected
      }
      return ''
    }

    const getHours = (day) => {
      const openingTimesMoment = openingTimes[day.isoWeekday() - 1].map(toMoment(day))
      const chunks = _.chunk(openingTimesMoment, 2)

      function toMoment(_day) {
        return (time) => moment(_day).hour(_.split(time, ':', 2)[0]).minute(_.split(time, ':', 2)[1])
      }

      return chunks.map((chunk, i) => {
        // Ignore if one hour alone without closing
        if (chunk.length < 2) return null

        const hours = moment.range(_.first(chunk), _.last(chunk)).toArray('hours').map(hour => (
          <div
            className={`${styles.hour} ${getHourClass(hour)}`}
            key={hour.format()}
            onClick={this.handleSelection}
            onMouseEnter={this.extendSelection}
            data-start={hour.format()}
            data-end={moment(hour).add(1, 'hour').format()}
          >
            <span className={styles.label}>{hour.format('HH:mm')}</span>
          </div>
        ))

        // Fill closed chunks
        // Fill the closed time before
        if (_.first(chunks[0]).isAfter(toMoment(day)(firstOpenTime))) {
          const span = _.first(chunks[0]).diff(toMoment(day)(firstOpenTime), 'hours')
          hours.unshift(
            <div style={{ flex: span }} className={`${styles.hour} ${styles.closed}`} key={toMoment(day)(firstOpenTime).format()}></div>
          )
        }

        // Fill the closed time after
        // #1 until next chunk
        if (chunks[i + 1]) {
          const span = chunks[i + 1][0].diff(_.last(chunk).add(1, 'hour'), 'hours')
          hours.push(
            <div style={{ flex: span }} className={`${styles.hour} ${styles.closed}`} key={_.last(chunk).add(1, 'hour').format()}></div>
          )
        } else if (_.last(chunk).isBefore(toMoment(day)(latestOpenTime))) {
          // #2 until end
          const span = toMoment(day)(latestOpenTime).diff(_.last(chunk), 'hours')
          hours.push(
            <div style={{ flex: span }} className={`${styles.hour} ${styles.closed}`} key={_.last(chunk).add(1, 'hour')}></div>
          )
        }

        return hours
      })
    }
    const calendar = moment.range(
      moment().isoWeek(week).startOf('isoWeek'),
      moment().isoWeek(week).endOf('isoWeek')
    ).toArray('days').filter(this.isDayOpen, this).map(day => (
      <div className={styles.day} key={day.format('YYYY-MM-DD')}>
        <div className={styles.head}>{day.format('LL')}</div>
        <div className={styles.hours}>{getHours(day)}</div>
      </div>
    ))


    return (
      <div className={styles.weeklyCalendar}>
        {calendar}
      </div>
    )
  }
}

WeeklyCalendar.propTypes = {
  week: React.PropTypes.number,
  startSelection: React.PropTypes.func,
  endSelection: React.PropTypes.func,
  extendSelection: React.PropTypes.func,
  closedWeekDays: React.PropTypes.arrayOf(React.PropTypes.number),
  closedDays: React.PropTypes.arrayOf(React.PropTypes.string),
  openingTimes: React.PropTypes.arrayOf(React.PropTypes.arrayOf(React.PropTypes.string)),
  selecting: React.PropTypes.bool,
  selectionStart: React.PropTypes.instanceOf(moment),
  selectionEnd: React.PropTypes.instanceOf(moment),
}

WeeklyCalendar.defaultProps = {
  week: moment().isoWeek(),
  closedWeekDays: [],
  closedDays: [],
  openingTimes: [
    ['08:00', '12:00', '14:00', '18:00'],
    ['08:00', '12:00', '14:00', '18:00'],
    ['08:00', '12:00'],
    ['08:00', '12:00', '14:00', '18:00'],
    ['14:00', '18:00'],
    ['08:00', '12:00', '14:00', '20:00'],
    [],
  ],
}

export default WeeklyCalendar
