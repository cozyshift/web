import WeeklyCalendar from '../index'

import expect from 'expect'
import { shallow } from 'enzyme'
import React from 'react'

/**
 * The date is force at test-bundler to be the 1 june 2015
 * We will use the week 23 - 2015
 * Which is Monday June 1, 2015 to Sunday June 7, 2015
 */

describe('<WeeklyCalendar />', () => {
  const openingTimes = [
    ['08:00', '12:00', '14:00', '18:00'],
    ['08:00', '12:00', '14:00', '18:00'],
    ['08:00', '12:00'],
    ['08:00', '12:00', '14:00', '18:00'],
    ['14:00', '18:00'],
    ['08:00', '12:00', '14:00', '18:00'],
    [],
  ]

  it('should render all opened day for the current date', () => {
    const renderedWeeklyCalendar = shallow(
      <WeeklyCalendar openingTimes={openingTimes} closedDays={['2015-06-03']} />
    )

    expect(renderedWeeklyCalendar.findWhere(n => n.key() === '2015-06-01').length).toEqual(1)
    expect(renderedWeeklyCalendar.findWhere(n => n.key() === '2015-06-02').length).toEqual(1)
    expect(renderedWeeklyCalendar.findWhere(n => n.key() === '2015-06-03').length).toEqual(0)
    expect(renderedWeeklyCalendar.findWhere(n => n.key() === '2015-06-04').length).toEqual(1)
    expect(renderedWeeklyCalendar.findWhere(n => n.key() === '2015-06-05').length).toEqual(1)
    expect(renderedWeeklyCalendar.findWhere(n => n.key() === '2015-06-06').length).toEqual(1)
    expect(renderedWeeklyCalendar.findWhere(n => n.key() === '2015-06-07').length).toEqual(0)
  })
  it('should render opened times', () => {
    const renderedWeeklyCalendar = shallow(
      <WeeklyCalendar
        openingTimes={openingTimes}
      />
    )

    const openingTimesNodes = [
      <div><span>08:00</span></div>,
      <div><span>09:00</span></div>,
      <div><span>10:00</span></div>,
      <div><span>11:00</span></div>,
      <div><span>12:00</span></div>,
      <div></div>,
      <div><span>14:00</span></div>,
      <div><span>15:00</span></div>,
      <div><span>16:00</span></div>,
      <div><span>17:00</span></div>,
      <div><span>18:00</span></div>,
    ]

    const openingTimesNodesMorning = [
      <div><span>08:00</span></div>,
      <div><span>09:00</span></div>,
      <div><span>10:00</span></div>,
      <div><span>11:00</span></div>,
      <div><span>12:00</span></div>,
      <div></div>,
    ]

    const openingTimesNodesAfternoon = [
      <div></div>,
      <div><span>14:00</span></div>,
      <div><span>15:00</span></div>,
      <div><span>16:00</span></div>,
      <div><span>17:00</span></div>,
      <div><span>18:00</span></div>,
    ]

    expect(renderedWeeklyCalendar.findWhere(n => n.key() === '2015-06-01').containsAllMatchingElements(openingTimesNodes)).toBe(true)
    expect(renderedWeeklyCalendar.findWhere(n => n.key() === '2015-06-02').containsAllMatchingElements(openingTimesNodes)).toBe(true)
    expect(renderedWeeklyCalendar.findWhere(n => n.key() === '2015-06-03').containsAllMatchingElements(openingTimesNodesMorning)).toBe(true)
    expect(renderedWeeklyCalendar.findWhere(n => n.key() === '2015-06-04').containsAllMatchingElements(openingTimesNodes)).toBe(true)
    expect(renderedWeeklyCalendar.findWhere(n => n.key() === '2015-06-05').containsAllMatchingElements(openingTimesNodesAfternoon)).toBe(true)
    expect(renderedWeeklyCalendar.findWhere(n => n.key() === '2015-06-06').containsAllMatchingElements(openingTimesNodes)).toBe(true)
  })
  it('should render working employee')
})
