/**
*
* EmployeeWidget
*
*/

import React from 'react'

import styles from './styles.css'

import moment from 'moment'

function EmployeeWidget({
  amountTime,
  capacity,
  firstname,
  lastname,
}) {
  function getInitial(string) {
    const matches = string.match(/\b(\w)/g)
    return matches.join(' ')
  }
  return (
    <div
      className={`${styles['radial-progress']} ${styles.half}`}
      data-progress={80} data-max={capacity}
    >
      <div className={styles['inset-head']}></div>
      <div className={styles.circle}>
        <div className={styles.mask}>
          <div className={styles.fill}></div>
        </div>
        <div className={styles.badge} style={{ backgroundColor: 'darkblue' }}></div>
      </div>
      <div className={styles.inset}>
        <div className={styles.portrait}>
          <span className={styles['icon-user']}></span>
        </div>
        <div className={styles.initial}>
          <span>{getInitial(`${firstname} ${lastname}`)}</span>
        </div>
      </div>
      <div className={styles.label}>
        <span className={styles.name}>{name}</span>
      </div>
      <div className={styles.label2}>
        <span className={styles.hours}>
          {`${amountTime}h`} / {`${capacity}h`}
        </span>
      </div>
    </div>
  )
}

EmployeeWidget.propTypes = {
  id: React.PropTypes.string.isRequired,
  firstname: React.PropTypes.string.isRequired,
  lastname: React.PropTypes.string.isRequired,
  capacity: React.PropTypes.number.isRequired,
  // color: React.PropTypes.string.isRequired,
  amountTime: React.PropTypes.number.isRequired,
}

EmployeeWidget.defaultProps = {
}


export default EmployeeWidget
