import EmployeeWidget from '../index'

import expect from 'expect'
import { shallow } from 'enzyme'
import React from 'react'

import moment from 'moment'

describe('<EmployeeWidget />', () => {
  const EmployeeWidgetDom = shallow(
    <EmployeeWidget
      id="31469636-86a3-4bd7-bfa3-8f6f6d3ca32b"
      firstname="max"
      lastname="musterman"
      workingTime={35}
      events={[
        { start: moment('2016-06-01T08:00:00'), end: moment('2016-06-01T12:00:00') },
        { start: moment('2016-06-01T15:00:00'), end: moment('2016-06-01T18:00:00') },
        { start: moment('2016-06-02T08:00:00'), end: moment('2016-06-01T12:00:00') },
        { start: moment('2016-06-03T08:00:00'), end: moment('2016-06-01T12:00:00') },
      ]}
    />
  )

  it('should render the hours worked in the current week vs maximum amount', () => {
    expect(EmployeeWidgetDom.containsMatchingElement(<span>15h / 35h</span>)).toEqual(true)
  })
  it('should render the employee name', () => {
    expect(EmployeeWidgetDom.containsMatchingElement(<span>Max Musterman</span>)).toEqual(true)
  })
  it('should render the color depending on his function', () => {

  })
  it('should render initial when no profile image', () => {

  })
})
