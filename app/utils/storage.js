export function storeItem(...item) {
  if (typeof(localStorage) !== undefined) {
    localStorage.setItem(...item)
  }
}

export function clearItem(item) {
  if (typeof(localStorage) !== undefined) {
    localStorage.removeItem(item)
  }
}

export function getItem(item) {
  if (typeof(localStorage) === undefined) {
    return false
  }

  return localStorage.getItem(item)
}
