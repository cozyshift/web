import request from './request'
import * as storage from './storage'
import { API_URL } from 'config'

const token = storage.getItem('token')

export function graphql(query, data) {
  return request(`${API_URL}/graphql`, {
    method: 'post',
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify({
      query,
      variables: data,
    }),
  })
}

export default graphql
