/**
 * Test  sagas
 */

import expect from 'expect'
import { take, call, put, select } from 'redux-saga/effects'
import { getEmployeeList } from '../sagas'
import {
  FETCH_EMPLOYEE,
  FETCH_EMPLOYEE_SUCCESS,
  FETCH_EMPLOYEE_ERROR,
} from '../constants'
import { fetchEmployeeList } from 'api.js'

const generator = getEmployeeList()

describe('Employee Saga', () => {
  beforeEach(() => {
    expect(generator.next().value).toEqual(take(FETCH_EMPLOYEE))
    expect(generator.next(take(FETCH_EMPLOYEE)).value).toEqual(call(fetchEmployeeList))
  })
  it('should dispatch FETCH_EMPLOYEE_SUCCESS if it requests the data successfully', () => {
    const response = {
      1: { name: 'Musterman', firstname: 'Max', role: 'MANAGER' },
      2: { name: 'Moon', firstname: 'Alex', role: 'MANAGER' },
    }

    expect(generator.next(response).value).toEqual(put({ type: FETCH_EMPLOYEE_SUCCESS, employee: response }))
  })
  it('should dispatch FETCH_EMPLOYEE_ERROR if the request failed', () => {
    const response = {
      err: 'Ouups Something happened !',
    }

    expect(generator.next(response).value).toEqual(put({ type: FETCH_EMPLOYEE_ERROR, error: response.err }))
  })
})
