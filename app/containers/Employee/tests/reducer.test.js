import expect from 'expect'
import employeeReducer from '../reducer'
import { fromJS } from 'immutable'

describe('employeeReducer', () => {
  it('returns the initial state', () => {
    expect(employeeReducer(undefined, {})).toEqual(fromJS({}))
  })
})
