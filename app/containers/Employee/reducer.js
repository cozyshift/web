/*
 *
 * Employee reducer
 *
 */

import { fromJS } from 'immutable'
import {
  FETCH_EMPLOYEE,
  FETCH_EMPLOYEE_SUCCESS,
  FETCH_EMPLOYEE_ERROR,
} from './constants'

const initialState = fromJS({
  byId: {},
  all: [],
  functionsById: {},
  loading: false,
  error: null,
})

function employeeReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_EMPLOYEE:
      return state.set('loading', true)
    case FETCH_EMPLOYEE_SUCCESS:
      return state.set('byId', fromJS(action.employees.entities.employees))
        .set('all', fromJS(action.employees.result))
        .set('loading', false)
    case FETCH_EMPLOYEE_ERROR:
      return state.set('error', action.error)
        .set('loading', false)
    default:
      return state
  }
}

export default employeeReducer
