import graphql from 'utils/graphql'
import { normalize, arrayOf, Schema } from 'normalizr'

const employee = new Schema('employees', { idAttribute: '_id' })
const type = new Schema('types', { idAttribute: '_id' })
const event = new Schema('events', { idAttribute: '_id' })

employee.define({
  type,
  events: arrayOf(event),
})

export function fetchEmployeeList() {
  return graphql(`
      query {
        employees {
          _id,
          firstname,
          lastname,
          email,
          type {
            _id,
            name,
            color
          },
          events {
            _id,
            start,
            end
          }
        }
      }
    `).then(response => {
      if (response.err) return response
      return { data: normalize(response.data.employees, arrayOf(employee)) }
    })
}
