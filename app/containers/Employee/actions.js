/*
 *
 * Employee actions
 *
 */

import {
  FETCH_EMPLOYEE,
  FETCH_EMPLOYEE_SUCCESS,
  FETCH_EMPLOYEE_ERROR,
} from './constants'

export function fetchEmployee() {
  return {
    type: FETCH_EMPLOYEE,
  }
}

export function fetchEmployeeSuccess(employees) {
  return {
    type: FETCH_EMPLOYEE_SUCCESS,
    employees,
  }
}

export function fetchEmployeeError(error) {
  return {
    type: FETCH_EMPLOYEE_ERROR,
    error,
  }
}
