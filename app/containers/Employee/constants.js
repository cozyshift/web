/*
 *
 * Employee constants
 *
 */

export const FETCH_EMPLOYEE = 'app/Employee/FETCH_EMPLOYEE'
export const FETCH_EMPLOYEE_SUCCESS = 'app/Employee/FETCH_EMPLOYEE_SUCCESS'
export const FETCH_EMPLOYEE_ERROR = 'app/Employee/FETCH_EMPLOYEE_ERROR'
