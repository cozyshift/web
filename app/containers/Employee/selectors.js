import { createSelector } from 'reselect'

/**
 * Direct selector to the employee state domain
 */
const selectEmployeeDomain = () => state => state.get('employee')

/**
 * Other specific selectors
 */


/**
 * Default selector used by Employee
 */

const selectAllEmployees = () => createSelector(
  selectEmployeeDomain(),
  substate => {
    return substate.get('all').map(employee => substate.get('byId').get(employee))
  }
)

export {
  selectAllEmployees,
}
