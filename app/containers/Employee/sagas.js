import { take, call, put } from 'redux-saga/effects'
import { fetchEmployeeList } from './api.js'

import {
  FETCH_EMPLOYEE,
} from './constants'

import {
  fetchEmployeeSuccess,
  fetchEmployeeError,
} from './actions'

// All sagas to be loaded
export default [
  getEmployeeList,
]

// Individual exports for testing
export function* getEmployeeList() {
  while (yield take(FETCH_EMPLOYEE)) {
    const employees = yield call(fetchEmployeeList)
    if (employees.err === undefined || employees.err === null) {
      yield put(fetchEmployeeSuccess(employees.data))
    } else {
      yield put(fetchEmployeeError(employees.err))
    }
  }
}
