/*
 *
 * Employee
 *
 */

import React from 'react'
import { connect } from 'react-redux'
import { selectAllEmployees } from './selectors'
import { createSelector } from 'reselect'
import { fetchEmployee } from './actions'
import { List } from 'immutable'

export class Employee extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentWillMount() {
    this.props.fetchEmployee()
  }

  render() {
    const renderEmployees = this.props.employees.map(employee => (
      <li key={employee.get('_id')}>
        <label htmlFor={employee.get('_id')} >
          <input id={employee.get('_id')} onChange={this.props.selectEmployee} type="radio" />
          {employee.get('firstname')} {employee.get('lastname')}
        </label>
      </li>
    ))
    return (
      <ul>
        {renderEmployees}
      </ul>
    )
  }
}

Employee.propTypes = {
  fetchEmployee: React.PropTypes.func,
  employees: React.PropTypes.instanceOf(List),
  selectEmployee: React.PropTypes.func,
}

Employee.defaultProps = {
  selectEmployee: () => {},
}

const mapStateToProps = createSelector(
  selectAllEmployees(),
  employees => ({
    employees,
  })
)

function mapDispatchToProps(dispatch) {
  return {
    fetchEmployee: () => dispatch(fetchEmployee()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Employee)
