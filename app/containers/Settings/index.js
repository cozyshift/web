/*
 *
 * Settings
 *
 */

import React from 'react'
import { connect } from 'react-redux'
import selectSettings from './selectors'

export class Settings extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
      This is Settings container !
      </div>
    )
  }
}

const mapStateToProps = selectSettings()

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings)
