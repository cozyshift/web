import { createSelector } from 'reselect'

/**
 * Direct selector to the settings state domain
 */
const selectSettingsDomain = () => state => state.get('settings')

/**
 * Other specific selectors
 */


/**
 * Default selector used by Settings
 */

const selectSettings = () => createSelector(
  selectSettingsDomain(),
  (substate) => substate.toJS()
)

export default selectSettings
export {
  selectSettingsDomain,
}
