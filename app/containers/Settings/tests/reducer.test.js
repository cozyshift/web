import expect from 'expect'
import settingsReducer from '../reducer'
import { fromJS } from 'immutable'

describe('settingsReducer', () => {
  it('returns the initial state', () => {
    expect(settingsReducer(undefined, {})).toEqual(fromJS({}))
  })
})
