/*
 *
 * Auth actions
 *
 */

import {
  UPDATE_USERNAME,
  UPDATE_PASSWORD,
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGOUT,
} from './constants'

export function updatePassword(password) {
  return {
    type: UPDATE_PASSWORD,
    password,
  }
}

export function updateUsername(username) {
  return {
    type: UPDATE_USERNAME,
    username,
  }
}

export function login() {
  return {
    type: LOGIN,
  }
}

export function loginSuccess(token) {
  return {
    type: LOGIN_SUCCESS,
    token,
  }
}

export function loginError(error) {
  return {
    type: LOGIN_ERROR,
    error,
  }
}

export function logout() {
  return {
    type: LOGOUT,
  }
}
