import base64 from 'base-64'
import request from 'utils/request'

import {
  API_URL,
  API_SECRET,
  API_ID,
} from 'config'

export function authorize(username, password) {
  const encodedAuth = base64.encode(`${API_ID}:${API_SECRET}`)

  return request(`${API_URL}/oauth2/token`, {
    method: 'post',
    headers: {
      Authorization: `Basic ${encodedAuth}`,
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify({
      username,
      password,
      grant_type: 'password',
    }),
  })
}
