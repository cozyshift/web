/*
 *
 * Auth reducer
 *
 */

import { fromJS } from 'immutable'
import {
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGOUT,
  UPDATE_USERNAME,
  UPDATE_PASSWORD,
} from './constants'

const initialState = fromJS({
  username: '',
  password: '',
  loading: false,
  error: null,
  loggedIn: false,
  token: false,
})

function authReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN:
      return state.set('loading', true)
    case LOGIN_SUCCESS:
      return state.set('loading', false)
        .set('token', action.token)
        .set('error', null)
        .set('password', '')
        .set('loggedIn', true)
    case LOGIN_ERROR:
      return state.set('loading', false)
        .set('error', action.error)
        .set('loggedIn', false)
        .set('token', null)
        .set('password', '')
    case LOGOUT:
      return state.set('loading', false)
        .set('loggedIn', false)
        .set('token', null)
        .set('password', '')
    case UPDATE_USERNAME:
      return state.set('username', action.username)
    case UPDATE_PASSWORD:
      return state.set('password', action.password)
    default:
      return state
  }
}

export default authReducer
