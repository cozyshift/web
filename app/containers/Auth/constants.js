/*
 *
 * Auth constants
 *
 */

export const LOGIN = 'app/Auth/LOGIN'
export const LOGIN_SUCCESS = 'app/Auth/LOGIN_SUCCESS'
export const LOGIN_ERROR = 'app/Auth/LOGIN_ERROR'
export const LOGOUT = 'app/Auth/LOGOUT'
export const LOGOUT_SUCCESS = 'app/Auth/LOGOUT_SUCCESS'
export const LOGOUT_ERROR = 'app/Auth/LOGOUT_ERROR'
export const UPDATE_USERNAME = 'app/Auth/UPDATE_USERNAME'
export const UPDATE_PASSWORD = 'app/Auth/UPDATE_PASSWORD'
