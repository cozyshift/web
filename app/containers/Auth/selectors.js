import { createSelector } from 'reselect'

/**
 * Direct selector to the auth state domain
 */
const selectAuthDomain = () => state => state.get('auth')

/**
 * Other specific selectors
 */


/**
 * Default selector used by Auth
 */

const selectAuth = () => createSelector(
  selectAuthDomain(),
  (substate) => substate.toJS()
)

const selectUsername = () => createSelector(
  selectAuthDomain(),
  authState => authState.get('username')
)

const selectPassword = () => createSelector(
  selectAuthDomain(),
  authState => authState.get('password')
)

const selectError = () => createSelector(
  selectAuthDomain(),
  authState => authState.get('error')
)

const selectToken = () => createSelector(
  selectAuthDomain(),
  authState => authState.get('token')
)

const selectLoggedIn = () => createSelector(
  selectAuthDomain(),
  authState => authState.get('loggedIn')
)

export default selectAuth
export {
  selectAuthDomain,
  selectUsername,
  selectPassword,
  selectError,
  selectToken,
  selectLoggedIn,
}
