/*
 *
 * Auth
 *
 */

import React from 'react'
import { connect } from 'react-redux'
import { createSelector } from 'reselect'
import shallowCompare from 'react-addons-shallow-compare'
import { push } from 'react-router-redux'

import {
  selectUsername,
  selectError,
  selectPassword,
  selectLoggedIn,
} from './selectors'

import {
  login,
  logout,
  updateUsername,
  updatePassword,
} from './actions'

export class Auth extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentWillReceiveProps(nextProps) {
    if (!nextProps.isLoggedIn && this.props.isLoggedIn !== nextProps.isLoggedIn) {
      this.props.changeRoute('/')
    }
  }
  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState)
  }
  render() {
    return this.props.isLoggedIn ? (
      <div>
        {this.props.username}
        <button onClick={this.props.logout}>LOGOUT</button>
      </div>
    ) : (
      <form onSubmit={this.props.login}>
        <input
          type="text"
          onChange={this.props.updateUsername}
          value={this.props.username}
          placeholder="Username"
        />
        <input
          type="password"
          onChange={this.props.updatePassword}
          value={this.props.password}
          placeholder="Password"
        />
        <button onClick={this.props.login}>Login</button>
      </form>
    )
  }
}

Auth.propTypes = {
  updateUsername: React.PropTypes.func,
  updatePassword: React.PropTypes.func,
  login: React.PropTypes.func,
  logout: React.PropTypes.func,
  username: React.PropTypes.string,
  password: React.PropTypes.string,
  isLoggedIn: React.PropTypes.bool,
  changeRoute: React.PropTypes.func,
}

const mapStateToProps = createSelector(
  selectUsername(),
  selectPassword(),
  selectError(),
  selectLoggedIn(),
  (username, password, error, loggedIn) => ({ username, password, error, isLoggedIn: loggedIn })
)

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    login: evt => { evt.preventDefault(); dispatch(login()) },
    logout: () => dispatch(logout()),
    updateUsername: evt => dispatch(updateUsername(evt.target.value)),
    updatePassword: evt => dispatch(updatePassword(evt.target.value)),
    changeRoute: (url) => dispatch(push(url)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth)
