import { select, take, call, put, fork, cancel } from 'redux-saga/effects'

import {
  selectUsername,
  selectPassword,
} from './selectors'

import * as storage from 'utils/storage.js'
import * as api from './api'
import {
  loginSuccess,
  loginError,
  updateUsername,
} from './actions'

import {
  LOGIN,
  LOGOUT,
  LOGIN_ERROR,
} from './constants'

// All sagas to be loaded
export default [
  login,
  init,
]

export function* doLogin() {
  const username = yield select(selectUsername())
  const password = yield select(selectPassword())

  const response = yield call(api.authorize, username, password)

  if (response.err) {
    yield put(loginError(response.err.message))
  } else {
    yield storage.storeItem('token', response.access_token.value)
    yield storage.storeItem('username', username)
    yield put(loginSuccess(response.access_token.value))
  }
}

export function* login() {
  while (yield take(LOGIN)) {
    const watcher = yield fork(doLogin)
    const action = yield take([LOGIN_ERROR, LOGOUT])
    if (action.type === LOGOUT) {
      yield cancel(watcher)
    }

    yield call(storage.clearItem, 'token')
  }
}

export function* init() {
  const token = yield call(storage.getItem, 'token')
  const username = yield call(storage.getItem, 'username')

  if (username) {
    yield put(updateUsername(username))
  }

  if (username && token) {
    yield put(loginSuccess(token))
    yield take(LOGOUT)
    yield call(storage.clearItem, 'token')
  }
}
