/**
 * Test  sagas
 */

import expect from 'expect'
import { take, put, fork, call } from 'redux-saga/effects'
import { doLogin, login, loginWrapper } from '../sagas'
import * as api from 'api.js'

import {
  LOGIN,
  LOGIN_ERROR,
  LOGOUT,
} from '../constants'

import {
  loginError,
  loginSuccess,
} from '../actions'

const generator = login()

describe('loginFlow Saga', () => {
  const username = 'myusername'
  const password = 'mypassword'

  it('should run non-blocking Login and cancel if logout', () => {
    expect(generator.next().value).toEqual(fork(loginWrapper))
    expect(generator.next().value).toEqual(take(LOGOUT))
    // expect(generator.next(logout()).value).toEqual(generator2.next(logout()).value) Does not know how to test
    expect(generator.next(loginError('oops')).value).toEqual(call(api.clearItem, 'token'))
  })

  it('should dispatch LOGIN_SUCCESS when it requets successfully', () => {
    const response = {
      token: 'thisismysupersecrettoken',
    }
    expect(generator.next(response).value).toEqual(put(loginSuccess()))
  })

  it('should dispatch LOGIN_ERROR when it fails', () => {
    const response = {
      err: 'Oups Something went wrong',
    }
    expect(generator.next(response).value).toEqual(put(loginError(response.err)))
  })
})
