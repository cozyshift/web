/* eslint-disable */
import { take, call, put, select, fork, cancel } from 'redux-saga/effects'

import {
  LOAD_EVENTS,
  SAVE_EVENT,
} from './constants'

import { LOCATION_CHANGE } from 'react-router-redux'

import {
  loadEventsSuccess,
  loadEventsError,
} from './actions'

import {
  selectStart,
  selectEnd,
} from './selectors'

import {
  selectToken,
} from '../Auth/selectors'

import * as api from './api'

// All sagas to be loaded
export default [
  events,
  saveEvent,
]

// Individual exports for testing
export function* fetchEvents() {
  const start = yield select(selectStart())
  const end = yield select(selectEnd())

  const response = yield call(api.fetchEvents, start, end)
  if (response.err) {
    yield put(loadEventsError(response.err.message))
  } else {
    yield put(loadEventsSuccess(response.data))
  }
}

export function* eventsWatcher() {
  while (yield take(LOAD_EVENTS)) {
    yield call(fetchEvents)
  }
}

export function* events() {
  const watcher = yield fork(eventsWatcher)

  yield take(LOCATION_CHANGE)
  yield cancel(watcher)
}

export function* saveEventWatcher() {
  const response = yield api.saveEvent(event)

  if (response.err) {
    yield put(saveEventError(response.err.message))
  } else {
    yield put(saveEventSuccess(response.data))
  }
}

export function* saveEvent() {
  while (true) {
    const { event } = yield take(SAVE_EVENT)

    yield fork(saveEventWatcher)
  }
}
