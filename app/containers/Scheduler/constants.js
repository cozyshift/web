/*
 *
 * Scheduler constants
 *
 */

export const DEFAULT_ACTION = 'app/Scheduler/DEFAULT_ACTION'
export const LOAD_EVENTS = 'app/Scheduler/LOAD_EVENT'
export const LOAD_EVENTS_SUCCESS = 'app/Scheduler/LOAD_EVENT_SUCCESS'
export const LOAD_EVENTS_ERROR = 'app/Scheduler/LOAD_EVENT_ERROR'
export const START_SELECTION = 'app/Scheduler/START_SELECTION'
export const EXTEND_SELECTION = 'app/Scheduler/UPDATE_SELECTION'
export const END_SELECTION = 'app/Scheduler/END_SELECTION'
export const CANCEL_SELECTION = 'app/Scheduler/CANCEL_SELECTION'
export const SAVE_EVENT = 'app/Scheduler/SAVE_EVENT'
export const SAVE_EVENT_SUCCESS = 'app/Scheduler/SAVE_EVENT_SUCCESS'
export const SAVE_EVENT_ERROR = 'app/Scheduler/SAVE_EVENT_ERROR'
export const SELECT_EMPLOYEE = 'app/Scheduler/SELECT_EMPLOYEE'
export const UNSELECT_ALL_EMPLOYEE = 'app/Scheduler/UNSELECT_ALL_EMPLOYEE'
