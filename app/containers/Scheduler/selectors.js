import { createSelector } from 'reselect'

/**
 * Direct selector to the scheduler state domain
 */
const selectSchedulerDomain = () => state => state.get('scheduler')

/**
 * Other specific selectors
 */

/**
 * Default selector used by Scheduler
 */

const selectScheduler = () => createSelector(
  selectSchedulerDomain(),
  (substate) => substate.toJS()
)

const selectStart = () => createSelector(
  selectSchedulerDomain(),
  schedulerState => schedulerState.get('start')
)

const selectEnd = () => createSelector(
  selectSchedulerDomain(),
  schedulerState => schedulerState.get('end')
)

const selectEvents = () => createSelector(
  selectSchedulerDomain(),
  schedulerState => schedulerState.get('events')
)

const selectSelecting = () => createSelector(
  selectSchedulerDomain(),
  schedulerState => schedulerState.get('selecting')
)

const selectLoading = () => createSelector(
  selectSchedulerDomain(),
  schedulerState => schedulerState.get('loading')
)

const selectStartSelection = () => createSelector(
  selectSchedulerDomain(),
  schedulerState => schedulerState.get('startSelection')
)

const selectEndSelection = () => createSelector(
  selectSchedulerDomain(),
  schedulerState => schedulerState.get('endSelection')
)

export default selectScheduler
export {
  selectScheduler,
  selectStart,
  selectEnd,
  selectSchedulerDomain,
  selectEvents,
  selectSelecting,
  selectLoading,
  selectStartSelection,
  selectEndSelection,
}
