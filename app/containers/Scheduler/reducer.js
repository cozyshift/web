/*
 *
 * Scheduler reducer
 *
 */

import { fromJS } from 'immutable'
import moment from 'moment'
import {
  DEFAULT_ACTION,
  LOAD_EVENTS,
  LOAD_EVENTS_SUCCESS,
  LOAD_EVENTS_ERROR,
  START_SELECTION,
  EXTEND_SELECTION,
  END_SELECTION,
  CANCEL_SELECTION,
  SELECT_EMPLOYEE,
  SAVE_EVENT,
} from './constants'

const initialState = fromJS({
  loading: false,
  eventsById: {},
  allEvents: [],
  error: false,
  selecting: false,
  startSelection: null,
  endSelection: null,
  start: moment().startOf('week'),
  end: moment().endOf('week'),
  employeeId: null,
})

function schedulerReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state
    case LOAD_EVENTS:
      return state.set('loading', true)
        .set('error', false)
        .set('events', false)
    case LOAD_EVENTS_SUCCESS:
      return state.set('loading', false)
        .set('error', false)
        .set('eventsById', action.events.entities.events)
        .set('allEvents', action.events.result)
    case LOAD_EVENTS_ERROR:
      return state.set('loading', false)
        .set('error', action.error)
    case START_SELECTION:
      return state.set('selecting', true)
        .set('startSelection', action.startSelection)
        .set('endSelection', action.endSelection)
    case END_SELECTION:
      return state.set('endSelection', action.endSelection)
        .set('selecting', false)
    case EXTEND_SELECTION:
      return state.set('endSelection', action.endSelection)
    case CANCEL_SELECTION:
      return state.set('selecting', false)
        .set('startSelection', null)
        .set('endSelection', null)
    case SELECT_EMPLOYEE:
      return state.set('employeeId', action.employeeId)
    case SAVE_EVENT:
      state.get('eventsById').set(action.event._id, action.event)
      state.get('allEvents').push(action.event._id)
      return state
    default:
      return state
  }
}

export default schedulerReducer
