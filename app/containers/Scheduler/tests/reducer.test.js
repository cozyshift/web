import expect from 'expect'
import schedulerReducer from '../reducer'
import { fromJS } from 'immutable'

describe('schedulerReducer', () => {
  it('returns the initial state', () => {
    expect(schedulerReducer(undefined, {})).toEqual(fromJS({}))
  })
})
