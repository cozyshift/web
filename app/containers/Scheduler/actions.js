/*
 *
 * Scheduler actions
 *
 */

import {
  DEFAULT_ACTION,
  LOAD_EVENTS,
  LOAD_EVENTS_ERROR,
  LOAD_EVENTS_SUCCESS,
  START_SELECTION,
  EXTEND_SELECTION,
  END_SELECTION,
  CANCEL_SELECTION,
  SELECT_EMPLOYEE,
  SAVE_EVENT,
  SAVE_EVENT_SUCCESS,
  SAVE_EVENT_ERROR,
} from './constants'

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  }
}

export function loadEvents() {
  return {
    type: LOAD_EVENTS,
  }
}

export function loadEventsSuccess(events) {
  return {
    type: LOAD_EVENTS_SUCCESS,
    events,
  }
}

export function loadEventsError(error) {
  return {
    type: LOAD_EVENTS_ERROR,
    error,
  }
}

export function startSelection({ start, end }) {
  return {
    type: START_SELECTION,
    startSelection: start,
    endSelection: end,
  }
}

export function extendSelection({ end }) {
  return {
    type: EXTEND_SELECTION,
    endSelection: end,
  }
}

export function endSelection({ end }) {
  return {
    type: END_SELECTION,
    endSelection: end,
  }
}

export function cancelSelection() {
  return {
    type: CANCEL_SELECTION,
  }
}

export function selectEmployee(employeeId) {
  return {
    type: SELECT_EMPLOYEE,
    employeeId,
  }
}

export function saveEvent(event) {
  return {
    type: SAVE_EVENT,
    event,
  }
}

export function saveEventSuccess() {
  return {
    type: SAVE_EVENT_SUCCESS,
  }
}

export function saveEventError() {
  return {
    type: SAVE_EVENT_ERROR,
  }
}
