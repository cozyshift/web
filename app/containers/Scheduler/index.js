/*
 *
 * Scheduler
 *
 */

import React from 'react'
import { connect } from 'react-redux'
import moment from 'moment'
import { createSelector } from 'reselect'
import {
  selectEvents,
  selectLoading,
  selectStartSelection,
  selectEndSelection,
  selectSelecting,
} from './selectors'
import styles from './styles.css'

import {
  loadEvents,
  selectEmployee,
  startSelection,
  endSelection,
  extendSelection,
} from './actions'

import WeeklyCalendar from 'components/WeeklyCalendar'
import Employee from 'containers/Employee'

export class Scheduler extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentWillMount() {
    this.props.loadEvents()
  }

  render() {
    return (
      <div className={styles.scheduler}>
        <Employee selectEmployee={this.props.selectEmployee} />
        <WeeklyCalendar
          startSelection={this.props.startSelection}
          endSelection={this.props.endSelection}
          extendSelection={this.props.extendSelection}
          selecting={this.props.selecting}
          selectionStart={this.props.selectionStart}
          selectionEnd={this.props.selectionEnd}
        />
      </div>
    )
  }
}

Scheduler.propTypes = {
  loadEvents: React.PropTypes.func,
  selectEmployee: React.PropTypes.func,
  startSelection: React.PropTypes.func,
  endSelection: React.PropTypes.func,
  extendSelection: React.PropTypes.func,
  selecting: React.PropTypes.bool,
  selectionStart: React.PropTypes.instanceOf(moment),
  selectionEnd: React.PropTypes.instanceOf(moment),
}

function mapDispatchToProps(dispatch) {
  return {
    loadEvents: () => dispatch(loadEvents()),
    selectEmployee: ({ id }) => dispatch(selectEmployee(id)),
    startSelection: ({ start, end }) => dispatch(startSelection({ start, end })),
    endSelection: ({ end }) => dispatch(endSelection({ end })),
    extendSelection: ({ end }) => dispatch(extendSelection({ end })),
    dispatch,
  }
}

export default connect(createSelector(
  selectEvents(),
  selectLoading(),
  selectStartSelection(),
  selectEndSelection(),
  selectSelecting(),
  (events, loading, selectionStart, selectionEnd, selecting) => ({
    events,
    loading,
    selectionStart,
    selectionEnd,
    selecting,
  })
), mapDispatchToProps)(Scheduler)
