import graphql from 'utils/graphql'
import { normalize, arrayOf, Schema } from 'normalizr'

const employee = new Schema('employees', { idAttribute: '_id' })
const type = new Schema('types', { idAttribute: '_id' })
const event = new Schema('events', { idAttribute: '_id' })

event.define({
  employee,
})

employee.define({
  type,
})

export function fetchEvents(start, end) {
  return graphql(`query ($start: String!, $end: String!) {
      events (start: $start, end: $end) {
        _id,
        start,
        end,
        employee {
          _id,
          firstname,
          lastname,
          type {
            _id,
            color
          }
        }
      }
    }`,
    {
      start,
      end,
    }).then(response => {
      if (response.err) return response
      return { data: normalize(response.data, arrayOf(event)) }
    })
}

export function saveEvent(data) {
  return graphql('mutation ($data: EventInput!) { addEvent(data: $data) }', {
    data,
  })
}
